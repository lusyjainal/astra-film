## Installation

Make sure you have setup react native environment [here](https://reactnative.dev/docs/environment-setup)

Clone this repo

```
$ git clone git@gitlab.com:lusyjainal/astra-film.git
$ cd astra-film
```

Install dependencies

```sh
$ npm install
```

If you're running iOS, make sure you install the pods

```sh
$ cd ios
$ pod install
```

Run android or ios

```
$ npm run android
```

```
$ npm run ios
``